#!/bin/bash

# Keycloak admin credentials
KEYCLOAK_ADMIN_USER="keycloak_manager"
KEYCLOAK_ADMIN_PASSWORD="1234"

# Keycloak server URL
KEYCLOAK_SERVER_URL="http://localhost:8080/auth"

# Keycloak realm name
KEYCLOAK_REALM_NAME="PC_BOB"

# Client configuration
CLIENT_NAME="my-flask-app"
CLIENT_SECRET=$(openssl rand -hex 32)
REDIRECT_URIS='["http://localhost:5000/*"]'
WEB_ORIGINS='["http://localhost:5000"]'

# Keycloak admin access token
TOKEN=$(curl -s -X POST \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -d "grant_type=password" \
  -d "client_id=admin-cli" \
  -d "username=$KEYCLOAK_ADMIN_USER" \
  -d "password=$KEYCLOAK_ADMIN_PASSWORD" \
  "$KEYCLOAK_SERVER_URL/realms/master/protocol/openid-connect/token" | awk -F '"' '/access_token/{print $4}')

echo "Token : $TOKEN"
# Create the client in Keycloak and extract the client ID
curl -s -X POST \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $TOKEN" \
  -d '{"clientId":"'"$CLIENT_NAME"'","enabled":true,"redirectUris":'"$REDIRECT_URIS"',"webOrigins":'"$WEB_ORIGINS"',"clientAuthenticatorType":"client-secret","secret":"'"$CLIENT_SECRET"'"}' \
  "$KEYCLOAK_SERVER_URL/admin/realms/$KEYCLOAK_REALM_NAME/clients" | awk -F '"' '/id/{print $4}'


huho=$(curl -X GET \
  -H "Authorization: Bearer $TOKEN" \
  "http://localhost:8080/auth/admin/realms/PC_BOB/clients")

# Retrieve client ID to build flask app
# echo "Client ID: $huho"
client_id=$(echo "$huho" | grep -o '"id":"[^"]*"' | cut -d '"' -f 4)
echo "Client ID: $client_id"
echo "$client_id" > "../root_api/client_id"
