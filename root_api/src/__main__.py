import os
from flask import Flask, redirect, render_template, request, session, url_for
from flask_oidc import OpenIDConnect

import requests
class FlaskApi:

    @staticmethod
    def run():
        app = Flask(__name__)
        app.config.update(
            {
                'OIDC_CLIENT_SECRETS': 'root_api/src/client_secrets.json',
                'SECRET_KEY': 'abcd'
            }
        )

        oidc = OpenIDConnect(app)


        @app.route('/', methods=('GET',))
        def home():
            return 'HOME'
        
        @app.route('/login', methods=['GET', 'POST'])
        def login():
            if request.method == 'POST':
                username = request.form['username']
                password = request.form['password']
                
                keycloak_url = ''
                data = {
                    'grant_type': 'password',
                    'client_id': 'flask_api',
                    'username': username,
                    'password': password
                }
                response = requests.post(keycloak_url, data=data)
                print(response)
                if response.status_code == 200:
                    return redirect('/protected')
                else:
                    return render_template('login.html', error='Invalid username or password')
            # elif GET
            return render_template('login.html')

        @app.route('/hello')
        def hello():
            if oidc.user_loggedin:
                return f'Welcome {oidc.user_getfield("email")}'
            else:
                return 'not logged in'
            
        @app.route('/protected_old')
        @oidc.require_login
        def protected_old():
            return f'Welcome {oidc.user_getfield("email")}'
        
        @app.route('/protected')
        def protected():
            if 'access_token' not in session:
                return redirect('/login')
            return render_template('protected.html')

        app.run('0.0.0.0', 5001)

if __name__ == '__main__':
    app = FlaskApi()
    app.run()
