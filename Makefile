run_keycloak:
	cd root_keycloak && docker run -p 8080:8080 --rm -it $$(docker build -t flask_kc:1.0.0 -q .)

run_flask:
	cd root_api && docker build -t flask_api:1.0.0 .
	docker run -p 5001:5001 flask_api:1.0.0